import React, {useCallback, useState} from 'react';
import {HelloComponent} from "./hello";
import {HelloFunction} from "./hello-function";
import {someHoc} from "./some-hoc";

function App() {
    let [counter, setCounter] = useState(1);
    const incrementCounter = () => {
        setCounter(counter++);
    };
    let [secondMessage, setSecondMessage] = useState({secondMessage: 'World'});
    const incrementMessage = () => {
        setSecondMessage({secondMessage: 'World'});
    };
    const callback = useCallback(() => {
        console.info('callback called');
    }, []);
    const SomeHoc = someHoc(HelloFunction, {secondMessage, callback, message: 'Hello'});

  return (
    <div className="App">
        <button onClick={incrementCounter}>Increment</button>
        <button onClick={incrementMessage}>Message</button>
        {SomeHoc}
      <HelloFunction message={"Hello "} secondMessage={secondMessage} callback={callback} />
    </div>
  );
}

export default App;
