import {HelloFunction} from "./hello-function";
import React from "react";

export function someHoc(WrappedComponent: Function, data: {message: string, secondMessage: {secondMessage: string} , callback: Function}) {
    return(
    <WrappedComponent message={data.message} secondMessage={data.secondMessage} callback={data.callback} />
    );
};
