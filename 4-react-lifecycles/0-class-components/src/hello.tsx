import React from "react";

export class HelloComponent extends React.Component<any, any> {
    private message: string;
    constructor(props: { message: string }) {
        super(props);
        this.message = props.message;
        this.state = {
            counter: 1,
        };
        setInterval(() => {
            const counter = this.state.counter+1;
            this.setState({counter});
        },1000);
    }

    /**
     * This will be deprecated
     */
    componentWillMount(): void {
        console.info('componentWillMount');
    }

    /**
     * This will be deprecated
     */
    componentDidMount(): void {
        console.info('componentDidMount');
    }

    /**
     * This will be deprecated
     */
    componentWillReceiveProps(nextProps: Readonly<any>, nextContext: any): void {
        console.info('componentWillReceiveProps');
    }

    shouldComponentUpdate(nextProps: Readonly<any>, nextState: Readonly<any>, nextContext: any): boolean {
        console.info('shouldComponentUpdate');
        return true;
    }

    /**
     * This will be deprecated
     */
    componentWillUpdate(nextProps: Readonly<any>, nextState: Readonly<any>, nextContext: any): void {
        console.info('componentWillUpdate');
    }

    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<any>, snapshot?: any): void {
        console.info('componentDidUpdate');
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | Iterable<React.ReactNode> | React.ReactPortal | boolean | null | undefined {
        console.info('render');
        const {counter} = this.state;
        return (<div>
            {this.message} : {counter}
        </div>);
    }
}
