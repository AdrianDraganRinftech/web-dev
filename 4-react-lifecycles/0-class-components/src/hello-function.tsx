import React, {useEffect, useState} from "react";

export function HelloFunction(props: {message: string, secondMessage: {secondMessage: string}, callback: Function}) {
    const [counter, setCounter] = useState(1);
    useEffect(() => {
        console.info('useEffect');
    }, [props.secondMessage, props.message]);

    setTimeout(props.callback, 2000);

    const {message, secondMessage} = props;

    return (
        <div>{message} {secondMessage.secondMessage} : {counter}</div>
    );
}

