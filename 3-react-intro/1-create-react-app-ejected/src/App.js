import React from 'react';
import {TestMe} from './TestMe.tsx';

function App() {
  return (
    <div className="App">
      Hello world!
      <TestMe/>
    </div>
  );
}

export default App;
