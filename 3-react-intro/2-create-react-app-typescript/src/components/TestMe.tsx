import {MouseEventHandler, useState} from "react";
import './TestMe.css';

interface TestMeProps {
    myInput: string,
    myOutput: MouseEventHandler
}

export function TestMe(props: TestMeProps) {
    const [myInputState] = useState(['sdasda']);
    const { myInput, myOutput} = props;
    return (<div className={'test-me'} onClick={myOutput}>{myInput} {myInputState}</div>);
}
