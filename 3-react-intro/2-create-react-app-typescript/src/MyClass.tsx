import * as React from "react";
import {MouseEventHandler} from "react";

export class MyClass extends React.Component<any, any> {
    props: {
        myInput: string,
        myOutput: MouseEventHandler
    }
    constructor(props: {
        myInput: string,
        myOutput: MouseEventHandler
    }) {
        super(props);
        this.props = props;
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | Iterable<React.ReactNode> | React.ReactPortal | boolean | null | undefined {
        return (<div onClick={this.props.myOutput}>
            {this.props.myInput}
        </div>);
    }
}
