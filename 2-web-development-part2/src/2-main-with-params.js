import {WcAutocomplete} from './wc-autocomplete/wc-autocomplete-params.js';

window.addEventListener('load', () => {
  const onKeyup = (value) => {
    fetch(`http://localhost:3000/libraries?q=${value}`).
        then(response => response.json()).
        then((data) => {
          autocompleteElement.results = data;
        });
  };
  const onItemClicked = console.info;

  const autocompleteElement = new WcAutocomplete({
    onKeyup: onKeyup,
    onItemClicked,
    minLength: 2,
    debounce: 3000
  });

  document.querySelector('#prefix-button').addEventListener('click', () => {
    autocompleteElement.setAttribute('prefix', 'Library');
  });
  document.querySelector('#web-component').appendChild(autocompleteElement);
});

