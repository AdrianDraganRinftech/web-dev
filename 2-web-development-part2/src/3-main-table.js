import {RGTable} from './table/table.component.js';

window.addEventListener('load', async () => {
  const data = await (await fetch(`http://localhost:3000/libraries`)).json();
  const columns = [{
    label: 'Title',
    key: 'title'
  },{
    label: 'Author',
    key: 'author'
  }];

  const tableComponent = new RGTable({columns,data});
  document.querySelector('#web-component').appendChild(tableComponent);
});

