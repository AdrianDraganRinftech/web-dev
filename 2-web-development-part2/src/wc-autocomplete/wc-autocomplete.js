const template = document.createElement('template');
template.innerHTML = `
    <style>
      input {
          color: white;
          background-color: var(--bg-color);
          margin: 0 -15px;
          width: 100%;
          height: var(--height-input);
          border-radius: calc(var(--height-input) / 2);
          border: 1px solid darkgray;
          padding: 0 17px;
      }
      
      input:focus {
          outline: none;
          background-color: #37383b;
      }
      .autocomplete-results {
          border: 1px solid gray;
          width: 100%;
          height: 100px;
          margin: 17px -15px;
          padding: 17px;
          border-radius: 17px;
          font-size: 20px;
      }
      .autocomplete-results > div {
          color: white;
          border-bottom: 1px solid white;
          cursor: pointer;
      }
      
      .autocomplete-results small {
          margin-left: 10px;
      }
      
      
      .autocomplete-results.show {
          display: block;
      }
      .autocomplete-results.hide {
          display: none;
      }
    </style>
    <input type="text">
    <div class="autocomplete-results hide"></div>
`;

class WcAutocomplete extends HTMLElement {
  #options;
  #inputElement;
  #resultListElement;

  constructor(options = {onItemClicked: console.info}) {
    super();
    this.#options = options;
    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.#inputElement = this.shadowRoot.querySelector('input');
    this.#resultListElement = this.shadowRoot.querySelector('.autocomplete-results');

    this.#bindEvents();
  }

  #bindEvents() {
    this.#inputElement.addEventListener('keyup', () => {
      this.#query(this.#inputElement.value).then(this.#buildResultList.bind(this));
    });
    this.#inputElement.addEventListener('blur', this.#hideResultList.bind(this));
    this.#inputElement.addEventListener('focus', this.#showResultList.bind(this));
  }
  #query(value) {
    return fetch(`http://localhost:3000/libraries?q=${value}`).then(response => response.json());
  }

  #buildResultList = (data) => {
    this.#showResultList();
    this.#resultListElement.innerHTML = '';
    data.forEach((item) => {
      const itemEl = this.#buildResultItem(item);
      itemEl.addEventListener('mousedown', () => this.#handleItemClick(item));
      this.#resultListElement.append(itemEl);
    });
  };
  #buildResultItem(item) {
    const itemEl = document.createElement('div');
    itemEl.classList.add('result-item');
    itemEl.append(item.title);
    const smallEl = document.createElement('small');
    smallEl.append(item.author);
    itemEl.append(smallEl);
    return itemEl;
  };

  #hideResultList() {
    this.#resultListElement.classList.remove('show');
    this.#resultListElement.classList.add('hide');
  };

  #showResultList() {
    this.#resultListElement.classList.add('show');
    this.#resultListElement.classList.remove('hide');
  };

  #handleItemClick(item) {
    this.#options.onItemClicked(item);
  };
}

window.customElements.define('auto-complete', WcAutocomplete);

export {WcAutocomplete};
