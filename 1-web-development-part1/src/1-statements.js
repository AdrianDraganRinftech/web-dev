/**
 * var statement
 */
x;
y = 1;
var a = 'a';
var a = 'abc';

/**
 * let variables
 */
let b = 'b';
let b = 'b';

/**
 * const
 */
const c = 'c';
c = 'd';
const c = 'c';

/**
 * Scope
 */
if (true) {
  var x = 5;
}
console.log(x);

if (true) {
  let y = 5;
}
console.log(y);

if (true) {
  const z = 5;
}
console.log(z);

/**
 * Hoisting
 */
console.log(x);
console.log(y);
var y = 3;


/**
 * Reference
 */
a = {};
b = {};
a.b = b;

delete b;

console.info('b is ', b);
console.info('a.b is ', a.b);
