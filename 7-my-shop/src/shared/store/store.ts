import { configureStore } from '@reduxjs/toolkit';
import shoppingCartReducer from '../shopping-cart/shoppingCartSlice';
import { api } from '../../core/api';

export const store = configureStore({
  reducer: {
    shoppingCart: shoppingCartReducer,
    [api.reducerPath]: api.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
