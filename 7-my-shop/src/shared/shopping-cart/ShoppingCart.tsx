import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Card, Col, Container, Modal, Row, Spinner,
} from 'react-bootstrap';
import { RootState } from '../store/store';
import { toggle } from './shoppingCartSlice';
import { useGetShoppingCartQuery } from '../../core/api';
import { Product } from '../../core/api/models/product';

function ShoppingCart() {
  const dispatch = useDispatch();
  const show = useSelector((state: RootState) => state.shoppingCart.show);
  const { data: shoppingCart, isLoading } = useGetShoppingCartQuery();

  return (
    <Modal show={show} fullscreen>
      <Modal.Header
        closeButton
        onHide={() => {
          dispatch(toggle());
        }}
      >
        <Modal.Title>Shopping Cart</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {isLoading
          ? (
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          )
          : (
            <Container>
              {((shoppingCart || { products: [] }).products).map((product: Product) => (
                <Row key={product.id}>
                  <Col xs={12}>
                    <Card>
                      <Card.Img variant="top" src={product.img} />
                      <Card.Body>
                        <Card.Title>{product.label}</Card.Title>
                        <Card.Text>{product.description}</Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              ))}
            </Container>
          )}
      </Modal.Body>
    </Modal>
  );
}

export default ShoppingCart;
