import { createSlice } from '@reduxjs/toolkit';

export interface ShoppingCartState {
  show: boolean
}

const initialState: ShoppingCartState = {
  show: false,
};

export const counterSlice = createSlice({
  name: 'shoppingCart',
  initialState,
  reducers: {
    toggle: (state) => {
      // eslint-disable-next-line no-param-reassign
      state.show = !state.show;
    },
  },
});

// Action creators are generated for each case reducer function
export const { toggle } = counterSlice.actions;

export default counterSlice.reducer;
