import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';

function Search() {
  return (
    <Typeahead
      id="main-search"
      className="d-flex flex-fill"
      onChange={() => {
      }}
      options={[]}
    />
  );
}

export default Search;
