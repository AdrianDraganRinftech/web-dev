import React from 'react';
import { ButtonGroup, Button, Spinner } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Category } from '../../core/api/models/category';
import { useGetCategoriesQuery, useLazyGetSubcategoriesQuery } from '../../core/api';

function Categories() {
  const { data: categories, isLoading } = useGetCategoriesQuery();
  const [trigger, subcategories] = useLazyGetSubcategoriesQuery();
  const navigate = useNavigate();

  const toggleSubCategories = (category: Category) => {
    trigger(category.id);
  };
  const goTo = (catg: Category) => {
    navigate(`/products/${catg.id}`);
  };

  const renderNavLink = (catg: Category) => (<Button key={catg.key} onClick={() => toggleSubCategories(catg)} variant="light">{catg.label}</Button>);
  const renderSubcategory = (catg: Category) => (<Button key={catg.key} onClick={() => goTo(catg)} variant="light">{catg.label}</Button>);

  return (
    <div>
      { isLoading
        ? (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        )
        : (
          <div>
            <ButtonGroup vertical>
              {(categories as Category[]).map((category) => renderNavLink(category))}
            </ButtonGroup>
            {subcategories.isSuccess
              ? (
                <ButtonGroup vertical>
                  {subcategories.data.subcategories.map((category) => renderSubcategory(category))}
                </ButtonGroup>
              )
              : null}
          </div>
        )}
    </div>
  );
}

export default Categories;
