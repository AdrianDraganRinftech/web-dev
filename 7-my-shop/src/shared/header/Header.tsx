import React from 'react';
import {
  Container, Nav, Navbar, NavDropdown,
} from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBasketShopping } from '@fortawesome/free-solid-svg-icons';
import { toggle } from '../shopping-cart/shoppingCartSlice';
import Search from '../search/Search';
import logo from '../../assets/svgs/logo.svg';
import './Header.scss';

function Header() {
  const dispatch = useDispatch();
  const toggleShoppingCart = () => {
    dispatch(toggle());
  };

  return (
    <Navbar bg="light" expand="lg" className="header d-flex flex-fill">
      <Container>
        <Navbar.Brand href="#home">
          <img src={logo} alt="Logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto flex-fill d-flex">
            <div className="d-flex flex-fill">
              <Search />
            </div>
            <div>
              <NavDropdown title="John Doe" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.1">Logout</NavDropdown.Item>
              </NavDropdown>
            </div>
            <div className="shopping-dropdown">
              <FontAwesomeIcon icon={faBasketShopping} onClick={() => toggleShoppingCart()} />
            </div>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;
