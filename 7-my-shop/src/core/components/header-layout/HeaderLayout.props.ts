import { ReactElement } from 'react';

export interface HeaderLayoutProps {
  top: ReactElement
  children: ReactElement
}
