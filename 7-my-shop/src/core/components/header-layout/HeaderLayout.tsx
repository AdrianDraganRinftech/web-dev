import React from 'react';
import { HeaderLayoutProps } from './HeaderLayout.props';

function HeaderLayout({ top, children }: HeaderLayoutProps) {
  return (
    <div>
      <div className="d-flex flex-1 flex-row">
        {top}
      </div>
      {children}
    </div>
  );
}

export default HeaderLayout;
