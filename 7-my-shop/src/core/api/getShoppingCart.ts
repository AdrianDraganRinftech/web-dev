import { EndpointBuilder } from '@reduxjs/toolkit/dist/query/endpointDefinitions';
import { ShoppingCart } from './models/shoppingCart';

export type ShoppingCartResponse = ShoppingCart;

export const getShoppingCart = (build: EndpointBuilder<any, any, any>) => build.query<ShoppingCartResponse, void>({
  query: () => 'shopping-cart',
  providesTags: (result) => {
    if (result) {
      return [result, { type: 'ShoppingCart', id: 'ShoppingCart' }];
    }
    return [{ type: 'ShoppingCart', id: 'ShoppingCart' }];
  },
});
