import { EndpointBuilder } from '@reduxjs/toolkit/dist/query/endpointDefinitions';
import { Category } from './models/category';

export type CategoryResponse = Category[];

export const getCategories = (build: EndpointBuilder<any, any, any>) => build.query<CategoryResponse, void>({
  query: () => 'categories',
  providesTags: (result) => {
    if (result) {
      return [...result.map(({ id }) => ({ type: 'Category' as const, id })), { type: 'Category', id: 'LIST' }];
    }
    return [{ type: 'Category', id: 'LIST' }];
  },
});

export const getSubcategories = (build: EndpointBuilder<any, any, any>) => build.query<Category, number>({
  query: (id) => `categories/${id}?_embed=subcategories`,
  providesTags: (result) => {
    if (result) {
      return [...result.subcategories.map(({ id }) => ({ type: 'Subcategories' as const, id })), { type: 'Subcategories', id: 'SUB_LIST' }];
    }
    return [{ type: 'Subcategories', id: 'SUB_LIST' }];
  },
});
