import { EndpointBuilder } from '@reduxjs/toolkit/dist/query/endpointDefinitions';
import { Product } from './models/product';

export type ProductResponse = Product[];

export const getProducts = (build: EndpointBuilder<any, any, any>) => build.query<ProductResponse, string>({
  query: (id) => `products/${id}?_embed=products`,
  providesTags: (result) => {
    if (result) {
      return [...result.map(({ id }) => ({ type: 'Products' as const, id })), { type: 'Product', id: 'LIST' }];
    }
    return [{ type: 'Product', id: 'LIST' }];
  },
});
