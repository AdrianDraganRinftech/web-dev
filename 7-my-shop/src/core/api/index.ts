import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { getCategories, getSubcategories } from './getCategories';
import { getProducts } from './getProducts';
import { getShoppingCart } from './getShoppingCart';

export const api = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:5000/' }),
  tagTypes: ['Category'],
  endpoints: (build) => ({
    getCategories: getCategories(build),
    getSubcategories: getSubcategories(build),
    getProducts: getProducts(build),
    getShoppingCart: getShoppingCart(build),
  }),
});

// eslint-disable-next-line import/prefer-default-export
export const {
  useGetCategoriesQuery,
  useLazyGetSubcategoriesQuery,
  useGetProductsQuery,
  useGetShoppingCartQuery,
} = api;
