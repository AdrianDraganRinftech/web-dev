export interface Product {
  subcategoryId: number,
  id: number,
  label: string,
  description: string,
  img: string,
}
