export interface Category {
  id: number,
  key: string,
  label: string,
  subcategories: Category[],
}
