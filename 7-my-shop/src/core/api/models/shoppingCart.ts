import { Product } from './product';

export interface ShoppingCart {
  id: number,
  products: Product[],
}
