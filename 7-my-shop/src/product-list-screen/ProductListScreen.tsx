import React from 'react';
import {
  Button, Card, Col, Container, Row, Spinner,
} from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useGetProductsQuery } from '../core/api';
import { Product } from '../core/api/models/product';

type RouteParams = {
  categoryId: string;
};

function ProductListScreen() {
  const { categoryId } = useParams<RouteParams>();
  const { data: products, isLoading } = useGetProductsQuery(categoryId || '');

  const addToCart = (product: Product) => {
    console.info(product);
  };
  return (
    <Container>
      {isLoading
        ? (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        )
        : (
          <Row>
            {(products as Product[]).map((product: Product) => (
              <Col key={product.id} xs={12} sm={6} lg={4}>
                <Card>
                  <Card.Img variant="top" src={product.img} />
                  <Card.Body>
                    <Card.Title>{product.label}</Card.Title>
                    <Card.Text>{product.description}</Card.Text>
                    <Button variant="primary">Details</Button>
                    <Button variant="primary" onClick={() => addToCart(product)}>Buy NOW!</Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        )}
    </Container>
  );
}

export default ProductListScreen;
