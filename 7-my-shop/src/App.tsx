import React from 'react';
import { Route, Routes } from 'react-router-dom';
import HeaderLayout from './core/components/header-layout/HeaderLayout';
import Header from './shared/header/Header';
import ShoppingCart from './shared/shopping-cart/ShoppingCart';

const HomeScreen = React.lazy(() => import('./home-screen/HomeScreen'));
const ProductListScreen = React.lazy(() => import('./product-list-screen/ProductListScreen'));

function App() {
  return (
    <div>
      <ShoppingCart />
      <HeaderLayout top={<Header />}>
        <Routes>
          <Route
            path="/"
            element={(
              <React.Suspense fallback={<>...</>}>
                <HomeScreen />
              </React.Suspense>
            )}
          />
          <Route
            path="/products/:subcategoryId"
            element={(
              <React.Suspense fallback={<>...</>}>
                <ProductListScreen />
              </React.Suspense>
            )}
          />
        </Routes>
      </HeaderLayout>
    </div>
  );
}

export default App;
