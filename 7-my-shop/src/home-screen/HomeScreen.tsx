import React from 'react';
import { Container } from 'react-bootstrap';
import Categories from '../shared/categories/Categories';
import ErrorBoundary from '../core/components/error-boundry/ErrorBoundry';

function HomeScreen() {
  return (
    <Container>
      <ErrorBoundary>
        <Categories />
      </ErrorBoundary>
    </Container>
  );
}

export default HomeScreen;
