import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface User {
    username: string;
}

export interface UserState {
    current: User | null;
}

const initialState: UserState = {
    current: null,
};

export const login = createAsyncThunk('user/login', async () => {
    return await new Promise<User>((resolve) => {
        setTimeout(() => {
            resolve({
                username: 'John Dow',
            });
        }, 3000);
    });
});

export const logout = createAsyncThunk('user/logout', async () => {
    return await new Promise<User | null>((resolve) => {
        setTimeout(() => {
            resolve(null);
        }, 3000);
    });
});

const userReducer = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: {
        [login.fulfilled.type]: (state, action: PayloadAction<User>) => {
            state.current = action.payload;
        },
        [logout.fulfilled.type]: (state, action: PayloadAction<User>) => {
            state.current = action.payload;
        },
    }
});

export default userReducer;
