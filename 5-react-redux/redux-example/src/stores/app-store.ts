import { configureStore } from '@reduxjs/toolkit';
import userReducer from "./user-reducer";

const appStore = configureStore({
    reducer: {
        user: userReducer.reducer,
    }
});

export default appStore;

export type RootState = ReturnType<typeof appStore.getState>
export type AppDispatch = typeof appStore.dispatch;
