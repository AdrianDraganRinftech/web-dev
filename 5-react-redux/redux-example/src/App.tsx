import React from 'react';
import logo from './logo.svg';
import './App.css';
import userReducer, {login, User} from "./stores/user-reducer";
import {useAppDispatch, useAppSelector} from "./stores/hooks";

interface Props {
  user: User;
}

function App() {
  const user = useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();

  console.info(user);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {user.current?.username}
        </p>
        <button onClick={() => {
          dispatch(login())
        }}>
          Login
        </button>
      </header>
    </div>
  );
}

export default App;
