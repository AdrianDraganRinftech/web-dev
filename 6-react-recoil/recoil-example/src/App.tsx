import React from 'react';
import {RecoilRoot} from "recoil";
import {CharacterCounter} from "./character-count/character-counter";
import {CharacterCount} from "./character-count/character-count";

function App() {
    const updateMultipleAtoms = () => {

    };
  return (
      <RecoilRoot>
        <div className="App">
            <button onClick={updateMultipleAtoms}></button>
          <CharacterCounter/>
          <CharacterCount/>
        </div>
      </RecoilRoot>
  );
}

export default App;
