import {useRecoilState, useRecoilValue} from "recoil";
import {charCountState} from "./character-selector";
import {textState} from "./character-atom";

const CharacterCount = () => {
    const [text] = useRecoilState(textState);

    return <>Character Count: {text.length}</>;
};

export { CharacterCount };
