import {useRecoilState} from "recoil";
import {textState} from "./character-atom";

const CharacterCounter = () => {
    const [text, setText] = useRecoilState(textState);

    const onChange = (event: any) => {
        setText(event.target.value);
    };

    return (
        <div>
            <input type="text" value={text} onChange={onChange} />
            <br />
            Echo: {text}
        </div>
    );
};

export { CharacterCounter };
